FROM alpine:latest
COPY . /app
WORKDIR /app/PingCount

CMD ./PingCount.sh http://localhost
